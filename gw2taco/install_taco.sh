#!/bin/bash

# ========================= DESCRIPTION =================================#
# Description: Installer part of the gw2taco_linux package
# Project Link: https://gitlab.com/namoninja/gw2taco_linux
# Author: Namo Ninja
# License: GNU GPL v3+
# Version: 1.5.1
# =======================================================================#

#=============================== VARS ===================================#
VERSION="1.5.1"

# Identify location of where script is located and !not where it started
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
GW2="$(dirname "${DIR}")"
cd "${DIR}"

SETTINGS="config/settings.conf"
LINKS="config/taco.links"

# Get info from ${LINKS}
# TacoPackName
TPN=(A B C D E F)
	for X in ${TPN[*]}
	do
		eval TPN$X=`grep ${X}name== ${LINKS} | sed 's/^.name==//'`;
	done
# TacoPackLocation
TPL=(A B C D E F)
	for X in ${TPL[*]}
	do
		eval TPL$X=`grep ${X}loc== ${LINKS} | sed 's/^.loc==//'`;
	done
# TacoPackSiteurl
TPS=(A B C D E F)
	for X in ${TPS[*]}
	do
		eval TPS$X=`grep ${X}site== ${LINKS} | sed 's/^.site==//'`;
	done
# DownloadLink
DL=(A1 B1 C1 D1 E1 E2 E3 E4 E5 E6 E7 E8 E9 E10 E11 E12 F1)
	for X in ${DL[*]}
	do
		eval DL$X=`grep $X== ${LINKS} | sed 's/^.*==//'`;
	done

# Define placeholders
INSTALLED="Not installed"
sDIST="---" # distribution
sDE="---" # desktop
sWINE="---" # wine
sOPAC="---" # opacity
sAUTO="---" # autostart
sADELAY="---" # autostarttime
sMDELAY="---" # manualstarttime
sINSTALLED="" # installed (packs)
sACTIVE="" # active (packs)
sMODE="---" # mode

# Define Colors
GC='\e[32m' # Green
RC='\e[91m'	# Red
YC='\e[93m' # Yellow
MC='\e[95m'	# Magenta
BC='\e[94m'	# Blue
CC='\e[96m'	# Cyan
NC='\e[0m'	# No Color

#============================ FUNCTIONS =================================#
#-------------------------- ERROR CHECKS--------------------------------#
check_error()
{
	local check="$1"
	local userinput="$2"
	local exitcode="$2"
	local repeat="$2"
	local section="$3"
	local file="$3"

	case $check in

		"confirm")
			if [[ "$userinput" =~ ^[Yy]$ ]]; then # YES

				if [[ "$section" == "finish" ]]; then
					exit 0
				fi

			elif [[ "$userinput" =~ ^[Nn]$ ]]; then # NO
				
				if [[ "$section" == "download" ]] || [[ "$section" == "install" ]] || \
					[[ "$section" == "uninstall" ]] || [[ "$section" == "uninstall" ]]; then
					taco_man
				elif [[ "$section" == "finish" ]]; then
					check_install
				else
					printf "${RC} Exiting...${NC}\n\v"
					exit 1
				fi

			else
				printf "${RC}" && read -rep "Enter (y/n): " -n 1 YESNO && printf "${NC}"
				check_error "confirm" "$YESNO" "$section"
			fi
		;;

		"input")
			printf "${RC}Incorrect value provided for one of the choices. Please try again.${NC}\n\v"
			exit 2
		;;

		"autostart")
			if [[ "$userinput" =~ ^[YyNn]$ ]]; then
				echo ""
			else
				printf "${RC}" && read -rep "Enter (y/n): " -n 1 AUTO && printf "${NC}"
				check_error "autostart" "$AUTO"
			fi
		;;

		"appfolder")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----FAILED to remove app folder (probably did not exist) [err:${NC} $exitcode ${RC}]${NC}\n\v"
			fi
		;;

		"download")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----Download Failed! [err:${NC} $exitcode ${RC}]${NC}\n\v"
				rm $file
				read -n 1 -s -r -p "Press any key to continue"
				check_install
			else
				printf "${GC}----DONE.${NC}\n\v"
			fi
		;;

		"extract")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----ERROR\n Something went wrong during extraction... [err:${NC} $exitcode ${RC}]${NC}\n\v"
				read -n 1 -s -r -p "Press any key to continue"
				check_install
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"activate")
			if [[ "${sINSTALLED}" =~ [$userinput] ]]; then
				echo ""
			else
				printf "${RC} The package needs to be installed before you can activate it!${NC}\n\v"
				sleep 3 && check_install
			fi
		;;

		"apt")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}  Something went wrong with apt [err:${NC} $exitcode ${RC}]\n\v${NC}"
				exit $exitcode
			else
				printf "${GC}  Install complete.${NC}\n\v"
			fi
		;;

		"pacman")
			if [ $exitcode -ne 0 ]; then
				printf "${RC} Something went wrong with pacman! [err:${NC} $exitcode ${RC}]${NC}\n\v"
				exit $exitcode
			else
				printf "${GC}----Install complete.${NC}\n\v"
			fi
		;;

		"tweaks")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----ERROR [err:${NC} $exitcode ${RC}]${NC}\n\v"
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"kwinrules")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----Something went wrong with kwinrules... [err:${NC} $exitcode ${RC}]${NC}\n\v"
				exit $exitcode
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"kwinrulesremove")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----FAILED to remove kwinrules (probably did not exist) [err:${NC} $exitcode ${RC}]${NC}\n\v"
			fi
		;;

		"disabled")
			printf "${RC}[DISABLED] incomplete function, temporarily disabled.\n"
			sleep 2 && taco_man
		;;

		"general")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}--- ERROR! [err:${NC} $exitcode ${RC}]${NC}\n\v"
			else
				printf "${GC}--- DONE! ${NC}\n\v"
			fi
		;;
	esac
}
#------------------------------------------------------------------------#
#--------------------------- DEFAULT EXPORTS ---------------------------#
export_defaults()
{
	# Export configs
	printf "${GC}** Exporting user info to${CC} ${SETTINGS}... ${NC}\n"

		printf "version=${VERSION}\ngw2dir=${GW2}\ntacodir=${DIR}\npicom=${DIR}/config/picom.conf\ndistribution=${DIST}\ndesktop=${DE}\ncompositor=${COMP}\nwine=${WINE}" > "${DIR}/${SETTINGS}"
			check_error "tweaks" "$?"
	
	printf "${GC}** Setting Defaults... ${NC}\n"

		printf "\nautostarttime=120" >> "${SETTINGS}"
		printf " Default autostart time set to 120s\n"

		printf "\nmanualstarttime=5" >> "${SETTINGS}"
		printf " Default manualstart time set to 5s\n"

		if [ "${DE}" == "KDE" ]; then
			printf "\nopacity=50" >> "${SETTINGS}"
			printf " Default opacity set to 50\n"
		else
			printf "\nopacity=0.5" >> "${SETTINGS}"
			printf " Default opacity set to 0.5\n"
		fi

		printf "\nmode=opacity" >> "${SETTINGS}"
		printf " Default Tweak MODE set to use OPACITY\n"

	check_error "tweaks" "$?"
	
	printf "\ninstalled=" >> "${SETTINGS}"
	printf "\nactive=" >> "${SETTINGS}"
}

#-----------------------------------------------------------------------#
#-------------------------TACO MENU TRANSPARENCY FIX --------------------#
fix_transparent_css()
{
	local css_location="$1"
	printf "${GC}** Applying Transparency Patch for Taco styles...${NC}\n"
	sed -z 's/contextmenu.*contextmenu:hover/contextmenu{text-transform:none;border:1px;border-color:#808080;background:#010101;opacity:0.7;}\ncontextmenu:hover/' -i ${css_location}/UI_*.css &&
	printf "${GC}--- DONE!${NC}\n\v"
}

#-----------------------------------------------------------------------#
#---------------------------- TACO TRAILS FIX -------------------------#
#fix_taco_trails() #sample code, needs testing/adjustments
# {
#	sudo apt install winetricks
#	sudo pacman -S --needed winetricks
#	`WINEPREFIX=${GW2}/data winetricks dlls d3dcompiler_47`
#}

#-----------------------------------------------------------------------#
#-------------------- DOWNLOAD/INSTALL/ACTIVATE TACO ------------------#
install_tacopack()
{
	# local files=(${1})
	# local links=(${2})
	# local loc=${3}
	# local type=${4}
	# local folder=${5}
	local i=1 # offset

	clear
	show_header "tacoman"

	# DOWNLOAD
	# read -rep "Download archive(s)? (y/n) " -n 1 YESNO
	# check_error "confirm" "$YESNO" "download"

	show_header "tacoman"
	printf "${GC}** Downloading...${CC} ${files[0]} ${NC}\n"

	if [[ "${type}" == "standalone" ]]; then

		if [[ -f "${loc}/${files[0]}" ]]; then #File exists
			printf "${GC}** NOTE:${CC} ${files[0]} ${GC}already exists, skipping it's download...${NC}\n\v"
		else
			wget -nv --show-progress --user-agent="Mozilla" "${links[0]}" -O "${loc}/${files[0]}"
			check_error "download" "$?" "${loc}/${files[0]}"
		fi

	elif [[ "${type}" == "addon" ]]; then

		if [[ -f "${TPLA}/${files[0]}" ]]; then #File exists
			printf "${GC}** NOTE:${CC} ${files[0]} ${GC}already exists, skipping it's download...${NC}\n\v"
		else
			wget -nv --show-progress --user-agent="Mozilla" "${links[0]}" -O "${TPLA}/${files[0]}"
			check_error "download" "$?" "${TPLA}/${files[0]}"
		fi

		while [[ ${i} != ${#links[@]} ]]; do
			if [[ -f "${loc}/${files[${i}]}" ]]; then #File exists
				printf "${GC}** NOTE:${CC} ${files[${i}]} ${GC}already exists, skipping it's download...${NC}\n\v"
			else
				printf "${GC}** Downloading...${CC} ${files[${i}]} ${NC}\n"
				wget -nv --show-progress --user-agent="Mozilla" "${links[${i}]}" -O "${loc}/${files[${i}]}"
				check_error "download" "$?" "${loc}/${files[${i}]}"
			fi
			((i++))
		done
		local i=1 #reset counter
	fi

	# EXTRACT
	if [[ -z "$(ls -A ./app/${folder})" ]]; then #Not installed
		read -rep "Install tacopack? (y/n) " -n 1 YESNO
		check_error "confirm" "$YESNO" "install"

		printf "${GC}** Extracting ${CC}${files[0]}${NC}...\n"
		if [[ "${type}" == "standalone" ]]; then # Extract tacopack
			
			unzip -q -o "${loc}/${files[0]}" -d "app/${folder}"
			check_error "extract" "$?"

		elif [[ "${type}" == "addon" ]]; then # Extract official-taco.zip

			unzip -q -o "${TPLA}/${files[0]}" -d "app/${folder}"
			check_error "extract" "$?"

			while [[ ${i} != ${#files[@]} ]]; do # Extract secondary addons
				printf "${GC}** Extracting ${CC}${files[${i}]}${NC}...\n"

				if [[ ${i} == [2,3,5,6,7,8,9] ]]; then # TISCAN PACK SKIPS
					printf "${RC}----Skipping because file containes backslashes...\n\v"
				else # Extract normally
					unzip -q -o "${loc}/${files[${i}]}" -d "app/${folder}/POIs"
					check_error "extract" "$?"
				fi
				((i++))
			done
		fi

		install_tweaks

	else
		printf "${GC}** NOTE: TacoPack${CC} ${folder} ${GC}already installed, skipping...${NC}\n\v"
	fi

	#ACTIVATE
	sed -i "s/active=.*/active=${folder}/" ${SETTINGS} &&
	printf "\n${GC}--- TACOPACK ${CC}${folder}${GC} ACTIVATED! ${NC}\n\v" ||
	printf "${GC}--- FAILED TO ACTIVATE TACOPACK ${folder} ${NC}\n\v"
}

#------------------------------------------------------------------------#
#--------------------------- TACO MAN ----------------------------------#
taco_man()
{
	# Counters
	i=0
	o=0
	tp=("${TPNA}" "${TPNB}" "${TPNC}" "${TPND}" "${TPNE}" "${TPNF}")

	# Header
	show_header "tacoman"
	printf "${NC}         Select an available ${YC}option${NC} OR ${CC}package${NC}\n"
	printf "${NC}      Pressing any other keys will refresh PacMan${NC}\n\v"
	printf "${YC}        OPTION: ${YC}[$o]${NC} Exit\n"  && ((++o))
	if [ "${INSTALLED}" == "Not installed" ]; then
		printf "${YC}        OPTION: ${YC}[$o]${NC} Restart\n"
	else
		printf "${YC}        OPTION: ${YC}[$o]${NC} Uninstall/Reset\n"
	fi
		((++o))
	printf "${YC}        OPTION: ${YC}[$o]${NC} Check for Update\n"  && ((++o))
	printf "${YC}        OPTION: ${YC}[$o]${NC} Change Settings\n\v"  && ((++o))

	# Setup folders
	if [ ! -d "tacopacks" ]; then
		mkdir -p tacopacks/{A,B,C,D,E,F}
	fi
	if [ ! -d "app" ]; then
		mkdir -p app/{A,B,C,D,E,F}
	fi

	# Check tacopacks
	for folder in tacopacks/*; do
		if [[ -z "$(ls -A ./$folder)" ]]; then
			printf "${RC}      NOTFOUND: ${YC}[$o]${RC} ${tp[$i]} ${NC}\n"
		else
			if [[ "${sACTIVE}" == "${TPN[$i]}" ]]; then
				printf "${BC}        ACTIVE: ${YC}[$o]${BC} ${tp[$i]} ${NC}\n"
			elif [[ "${sINSTALLED}" =~ [${TPN[$i]}] ]]; then
				printf "${GC}     INSTALLED: ${YC}[$o]${GC} ${tp[$i]} ${NC}\n"
			else
				printf "${CC}    DOWNLOADED: ${YC}[$o]${CC} ${tp[$i]} ${NC}\n"
			fi
		fi
		((++o)) && ((++i))
	done
	
	# Footer
	show_footer "tacoman"
	read -rep "[Enter #:]" -n 1 TACOMAN

	# Option
	case $TACOMAN in

		0) # EXIT --------------------------
			exit 0
		;;

		1)  # RESET ------------------------
			if [ "${INSTALLED}" == "Not installed" ]; then
				check_install
			else
				check_install "uninstall"
			fi
		;;

		2) # UPDATE ------------------------
			show_header "tacoman" "single"
			printf "${NC}This area is reserved for easy updating of the gw2taco_linux module.\n${NC}"
			printf "${YC}Feature is under development, currently you get notified if there is an update when run_taco.sh is initialized and can download the latest package here.\n\v${NC}"

			latest=`curl --silent --show-error "https://gitlab.com/api/v4/projects/20790591/repository/files/latest/raw?ref=master"`
			latest_ver=`printf "${latest}" | grep 'ver=' | sed 's/^.*=//'`
			latest_dl=`printf "${latest}" | grep 'dl=' | sed 's/^.*=//'`

			printf "${NC}Current version: ${VERSION}.${NC}\n"
			
			printf "${NC}Latest version: ${latest_ver}.${NC}\n"
			printf "${NC}Latest download: ${latest_dl}.${NC}\n\v"

			printf "${NC}Press ${YC}[1]${NC} to download latest package or any other key to return to main menu.${NC}\n\v"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # Update
					printf "${GC}** Downloading${CC} ${latest_ver} ${GC}*${NC}\n"
					wget -nv --show-progress "${latest_dl}"
					check_error "download" "$?"

					printf "${CC}File has been downloaded. Automatic install not yet implemented, please do a fresh install manually.${NC}\n\v"
					read -n 1 -s -r -p "Press any key to continue"

				;;

				*) # RETURN
					taco_man
				;;

			esac
		;;

		3) # CHANGE SETTING ----------------
			show_header "tacoman" "single"
			
			o=1
			printf "${NC}Here you can update gw2taco_linux configurations found in ${MC}settings.conf${NC}.\n\v"

			printf "${YC}        OPTION: ${YC}[$o]${NC} Toggle Mode\n"  && ((++o))
			printf "${YC}        OPTION: ${YC}[$o]${NC} Toggle AutoStart\n"  && ((++o))
			printf "${YC}        OPTION: ${YC}[$o]${NC} Switch WINE\n"  && ((++o))
			printf "${YC}        OPTION: ${YC}[$o]${NC} Change Opacity\n"  && ((++o))
			printf "${YC}        OPTION: ${YC}[$o]${NC} Change AutoDelay\n"  && ((++o))
			printf "${YC}        OPTION: ${YC}[$o]${NC} Change ManualDelay\n\v"  && ((++o))

			printf "${CC}   press any other key to return to main menu.\n\v${NC}"
			
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # MODE
					if [[ ${sMODE} == "transparency" ]]; then
						sed -i 's/mode=.*/mode=opacity/' ${SETTINGS}
					elif [[ ${sMODE} == "opacity" ]]; then
						sed -i 's/mode=.*/mode=transparency/' ${SETTINGS}
					fi
				;;

				2) # AUTOSTART
					if [[ ${sAUTO} == "true" ]]; then
						sed -i 's/autostart=.*/autostart=false/' ${SETTINGS}
					else
						sed -i 's/autostart=.*/autostart=true/' ${SETTINGS}
					fi
				;;

				3) # WINE
					o=1
					printf "${YC}        	OPTION: ${YC}[$o]${NC} ArmordVehicle's Portable Package\n"  && ((++o))
					printf "${YC}        	OPTION: ${YC}[$o]${NC} Native WINE\n"  && ((++o))
					printf "${YC}        	OPTION: ${YC}[$o]${NC} Lutris WINE\n"  && ((++o))

					read -rep "[Choose an option #:]" -n 1 VALUE
					case $VALUE in
						1) sed -i "s/wine=.*/wine=portable/" ${SETTINGS} ;;
						2) sed -i "s/wine=.*/wine=native/" ${SETTINGS} ;;
						3) sed -i "s/wine=.*/wine=lutris/" ${SETTINGS} ;;
					esac
				;;

				4) # OPACITY
					if [[ ${sDE} == "KDE" ]]; then
						read -rep "[Enter value between (0-100):]" VALUE
					else 
						read -rep "[Enter value between (0.0-1.0):]" VALUE
					fi
					sed -i "s/opacity=.*/opacity=${VALUE}/" ${SETTINGS}
				;;

				5) # AUTODELAY
					read -rep "[Enter value in seconds (0-10000s):]" VALUE
					sed -i "s/autostarttime=.*/autostarttime=${VALUE}/" ${SETTINGS}
				;;

				6) # MANUALDELAY
					read -rep "[Enter value in seconds (0-10000s):]" VALUE
					sed -i "s/manualstarttime=.*/manualstarttime=${VALUE}/" ${SETTINGS}
				;;

				*) # RETURN
					taco_man
				;;

			esac

			check_error "general" "${?}"
			sleep 2
			check_install
		;;

		4) # [A] GW2TACO OFFICAL ----------------------
			files="gw2taco-official.zip"
			links="${DLA1}"
			loc="${TPLA}"
			type="standalone"
			folder="A"

			show_header "tacoman" "single"
			printf "             ${CC} ${TPNA}\n\v"
			printf "           ${TPSA}\n\v${NC}"
			printf "This is the official TacO and standard install.\n${NC}"
			printf "This package is required for some of the other packs.\n\v${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1)	install_tacopack "${files[*]}" "${links[*]}" "${loc}" "${type}" "${folder}" ;;
				*)	taco_man ;;
			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		5) # [B] TEKKIT ----------------------
			files=("gw2taco-official.zip" "gw2taco-tekkit.zip")
			links=("${DLA1}" "${DLB1}")
			loc="${TPLB}"
			type="addon"
			folder="B"

			show_header "tacoman" "single"
			printf "            ${CC} ${TPNB} ${NC}\n"
			printf " ${CC} ${TPSB} ${NC}\n\v"
			printf "NOTE: Requires official GW2TacO, which will be downloaded automatically.\n${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1) install_tacopack "${files[*]}" "${links[*]}" "${loc}" "${type}" "${folder}" ;;
				*) taco_man ;;
			esac

			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		6) # [C] ReActiff ----------------------
			files="gw2taco-reactiff.zip"
			link="${DLC1}"
			loc="${TPLC}"
			type="standalone"
			folder="C"

			show_header "tacoman" "single"
			printf "          ${CC} ${TPNC}\n"
			printf "        ${TPSC} ${NC}\n\v"
			printf "NOTE: Self contained install. Does not require official GW2TacO package.\n${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1) install_tacopack "${files[*]}" "${links[*]}" "${loc}" "${type}" "${folder}" ;;
				*) taco_man ;;
			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		7) # [D] MegaPack ----------------------
			filse="gw2taco-megapack.zip"
			links="${DLD1}"
			loc="${TPLD}"
			type="standalone"
			folder="D"

			show_header "tacoman" "single"
			printf "           ${CC} ${TPND}\n"
			printf " ${TPSD}${NC}\n\v"
			printf "NOTE: Self contained package. Does not require official GW2TacO package.\n${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1) install_tacopack "${files[*]}" "${links[*]}" "${loc}" "${type}" "${folder}" ;;
				*) taco_man ;;
			esac

			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		8) # [E] TISCAN ----------------------
			files=("gw2taco-official.zip" "gw2taco-tiscan1.zip" "gw2taco-tiscan2.zip" "gw2taco-tiscan3.zip" "gw2taco-tiscan4.zip" \
			"gw2taco-tiscan5.zip" "gw2taco-tiscan6.zip" "gw2taco-tiscan7.zip" "gw2taco-tiscan8.zip" \
			"gw2taco-tiscan9.zip" "gw2taco-tiscan10.zip" "gw2taco-tiscan11.zip" "gw2taco-tiscan12.zip")
			links=("${DLA1}" "${DLE1}" "${DLE2}" "${DLE3}" "${DLE4}" "${DLE5}" "${DLE6}" "${DLE7}" "${DLE8}" "${DLE9}" "${DLE10}" "${DLE11}" "${DLE12}")
			loc="${TPLE}"
			type="addon"
			folder="E"
			
			show_header "tacoman" "single"
			printf "           ${CC} ${TPNE}\n"
			printf "     ${TPSE} ${NC}\n\v"
			printf "NOTE: Requires official GW2TacO, which will be downloaded automatically.\n${NC}"
			printf "NOTE: Has multiple packages.\n${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1) install_tacopack "${files[*]}" "${links[*]}" "${loc}" "${type}" "${folder}" ;;
				*) taco_man ;;
			esac

			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		9) # [F] CUSTOM PACK ----------------------
			files="gw2taco-custom.zip"
			links="${DLF1}"
			loc="${TPLF}"
			type="standalone"
			folder="F"

			show_header "tacoman" "single"
			printf "           ${CC}${TPNF}\n"
			printf "    ${TPSF}${NC}\n\v"
			printf "NOTE: Update ${LINKS} file and refresh this page to load the link.\n\v${NC}"
			show_footer "tacopack"

			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in
				1) install_tacopack "${files}" "${folder}" "${loc}" "${link}" "${type}";;
				*) taco_man;;
			esac

			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		*) check_install ;;

	esac
}
#------------------------------------------------------------------------#
#------------------------- CLEAR & DISPLAY HEADER-------------------------#
show_header()
{	
	local header="$1"
	local header2="$2"
	clear

	printf "${GC}===========================================================\n"
	if [ "$header" == "tacoman" ]; then
		printf ":   	   WELCOME TO THE${CC} GW2TacO_Linux${GC} MANAGER	          :\n"
	else
		printf ":   	   WELCOME TO THE${CC} GW2TacO_Linux${GC} INSTALLER	  :\n"
	fi
	printf ":   							  :\n"
	printf ":		     ( Version: ${YC}${VERSION}${GC} )		          :\n"
	printf ": 	${BC} https://gitlab.com/namoninja/gw2taco_linux${GC} 	  :\n"
	printf "===========================================================${NC}\n\v"

	if [ "$header" == "tacoman" ]; then

		printf "${GC} Distribution: ${NC}${sDIST}${GC} Desktop: ${NC}${sDE}${GC}\n"
		printf "${GC} Tweaks: ${NC}${INSTALLED}${GC} WINE: ${NC}${sWINE}${NC}\n"
		printf "${GC} Compositor: ${NC}${s4}${GC} WINE: ${NC}${sWINE}${NC}\n"
		if [[ ${sMODE} == "opacity" ]]; then
			printf "${GC} MODE: ${NC}${sMODE}${GC} OpacityLevel: ${NC}${sOPAC}\n"
		else
			printf "${GC} MODE: ${NC}${sMODE}${NC}\n"
		fi
		printf "${GC} Autostart: ${NC}${sAUTO}${GC} AutoDelay: ${NC}${sADELAY}${GC} ManualDelay: ${NC}${sMDELAY}${NC}\n\v"

		if [ "$header2" == "installer" ]; then
			printf "${GC}  --------------------${YC} Taco Installer${GC} --------------------${NC}\n\v"
		elif [ "$header2" == "complete" ]; then
			printf "${GC}  --------------------${YC} Taco Installer${GC} --------------------${NC}\n\v"
			printf "${GC}\n------------ Installation complete ------------${NC}"
		else
			printf "${GC}  --------------------${YC} Taco Manager${GC} --------------------${NC}\n\v"
		fi

	fi
}
#------------------------------------------------------------------------#
#--------------------------- DISPLAY FOOTER ----------------------------#
show_footer() {
	local footer="$1"

	if [ "$footer" == "tacoman" ]; then
		echo ""
		printf "${GC}  -----------------------------------------------------${NC}\n\v"
		printf "${YC} [OPTION] ${RC}[NOTFOUND] ${CC}[DOWNLOADED] ${GC}[INSTALLED] ${BC}[ACTIVE] ${NC}\n\v"
	
	elif [ "$footer" == "tacopack" ]; then
		printf "\n	${YC}[ 1 ]${GC} Install / Activate\n\v"
		printf "${GC}   or press any key to return to main menu.\n\v${NC}"
		printf "${GC}  -----------------------------------------------------${NC}\n\v"
	fi
}
#------------------------------------------------------------------------#
#------------------- CHECK IF INSTALLED AND OFFER RESET ----------------#
check_install()
{
	local action="$1"
	show_header "tacoman" "installer"

	# If ${SETTINGS} exists
	if [ -f "${SETTINGS}" ]; then
		
		# Store info in vars
		INSTALLED="Installed"
		sDIST=`grep 'distribution=' ${SETTINGS} | sed 's/^.*=//'`
		sDE=`grep 'desktop=' ${SETTINGS} | sed 's/^.*=//'`
		sWINE=`grep 'wine=' ${SETTINGS} | sed 's/^.*=//'`
		sOPAC=`grep 'opacity=' ${SETTINGS} | sed 's/^.*=//'`
		sAUTO=`grep 'autostart=' ${SETTINGS} | sed 's/^.*=//'`
		sADELAY=`grep 'autostarttime=' ${SETTINGS} | sed 's/^.*=//'`
		sMDELAY=`grep 'manualstarttime=' ${SETTINGS} | sed 's/^.*=//'`
		sINSTALLED=`grep 'installed=' ${SETTINGS} | sed 's/^.*=//'`
		sACTIVE=`grep 'active=' ${SETTINGS} | sed 's/^.*=//'`
		sMODE=`grep 'mode=' ${SETTINGS} | sed 's/^.*=//'`

		# If chooses to reset
		if [ "$action" == "uninstall" ]; then
			printf "${YC} Uninstall tweaks & reset tacoman?${NC}\n This will remove all configs and installed tacopacks.\n Downloaded tacopack.zips will remain for reuse.${NC}\n\v"

			read -rep "Continue? (y/n):" -n 1 YESNO
			check_error "confirm" "$YESNO" "uninstall"

			show_header "tacoman"
			printf "${GC}\v  REMOVING APP FOLDER${NC}\n"
			rm -r app
			check_error "appfolder" "$?"

			# If using portable package
			if [ "`cat ${SETTINGS} | grep -o portable`" == "portable" ]; then
				# Unlink from portable package
				printf "${GC}  REMOVING LINK FROM ${NC}../bin/user_run\n"
				sed -i 's/\ \&\ \.\.\/\.\.\/\.\.\/gw2taco\/run_taco.sh//' ../bin/user_run
			fi
			
			# If using KDE
			if [ "`cat ${SETTINGS} | grep -o KDE`" == "KDE" ]; then
				
				# Remove KDE Entries
				printf "${GC}  REMOVING kwinrules${NC}\n"
				COUNT=`grep 'kwinrule=' ${SETTINGS} | sed 's/^.*=//'`
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key Description --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactiverule --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key title --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key titlematch --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclass --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclasscomplete --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclassmatch --delete &
				kwriteconfig5 --file kwinrulesrc --group "General" --key count "$(($COUNT - 1))"
				check_error "kwinrulesremove" "$?"

			fi

			# Remove config
			printf "${GC}  REMOVING ${SETTINGS}${NC}\n"
			rm ${SETTINGS}

			printf "${GC}\nGW2Taco_Linux has been reset. Please re-run install_taco.sh${NC}\n\v"
			read -n 1 -s -r -p "Press any key to exit..."
			printf "${NC}\n\v"
			exit
			
		fi
		taco_man

	fi
	if [ "${sDIST}" == "---" ]; then
		collect_info
	fi
	taco_man
}
#------------------------------------------------------------------------#
#------------------------ START BY GATHERING INFO ----------------------#
collect_info()
{	
	#--- NOTICE
	printf "${GC}       	       ----------${YC} Note:${GC} ---------- \n" 
	printf "            ${YC} The project is in Beta testing. ${GC} \n"
	printf "${YC} If you encounter any errors, check the issue board on${BC} GitLab${GC} \n\v"

	#--- DISTRIBUTION
	printf "Before we proceed we need to know a few things...\n"
	printf "${GC}\nWhat distribution are you running?${NC}\n\v"
	printf "[1] Debian/Ubuntu/Mint\n"
	printf "[2] Arch/Manjaro\n\v"

	read -rep "Enter [#]: " -n 1 c1

	#--- DEKSTOP ENVIRONMENT
	show_header "tacoman" "installer"
	printf "${GC}\nWhat is your Desktop Environment?${NC}\n\v"
	printf "[1] XFCE\n"
	printf "[2] KDE\n"
	printf "[3] Cinnamon\n"
	printf "[4] GNOME\n"
	printf "[5] i3wm\n\v"

	read -rep "Enter [#]: " -n 1 c2

	#--- COMPOSITOR
	show_header "tacoman" "installer"
	printf "${GC}\nAre you using a custom compositor or the default one?${NC}\n"
	printf "(if you do not know what this means, choose default)\n\v"
	printf "[1] Native/Default\n"
	printf "[2] Picom/Compton\n"
	printf "[3] Other\n\v"

	read -rep "Enter [#]: " -n 1 c3

	#--- WINE
	show_header "tacoman" "installer"
	printf "${GC}\nWhich WINE binaries are you using?${NC}\n\v"
	printf "[1] ArmordVehicle's Portable Package\n"
	printf "[2] Native WINE\n"
	printf "[3] Lutris WINE\n\v"

	read -rep "Enter [#]: " -n 1 c4

	#======= CHOICE VARS ===============#
	case "${c1}" in						#
		1) DIST="Debian/Ubuntu/Mint";;	#	DISTRIBUTION
		2) DIST="Arch/Manjaro";;		#
		*) check_error "input";;		#
	esac								#	
	#-----------------------------------#
	case "${c2}" in						#
		1) DE="XFCE";;					#
		2) DE="KDE";;					#
		3) DE="Cinnamon";;				#	DESKTOP ENVIRONMENT
		4) DE="GNOME";;					#
		5) DE="i3wm";;					#
		*) check_error "input";;		#
	esac								#
	#-----------------------------------#
	case "${c3}" in						#
		1)								#
			case "${c2}" in				#
				1) COMP="xfwm";;		#
				2) COMP="kwin";;		#
				3) COMP="muffin";;		#
				4) COMP="mutter";;		#
				5) COMP="picom";;		#	COMPOSITOR
			esac						#
		;;								#
		2) COMP="picom" ;;				#
		3) COMP="other" ;;				#
		*) check_error "input" ;;		#
	esac								#
	#-----------------------------------#
	case "${c4}" in                     #
		1) WINE="portable";;			#
		2) WINE="native";;				#	WINE
		3) WINE="lutris";;				#
		*) check_error "input" ;;		#
	esac								#
	#===================================#

	#--------------------- OUTPUT SUMMARY -------------------------------#
	sDIST="${DIST}"
	sDE="${DE}"
	sWINE="${WINE}"
	show_header "tacoman" "installer"

	printf "${GC} Right, so you are on: ${CC}${DIST}${NC}\n"
	printf "${GC} Your desktop environment is: ${CC}${DE}${NC}\n"
	printf "${GC} The compositor is: ${CC}${COMP}${NC}\n"
	printf "${GC} and WINE binaries are: ${CC}${WINE}${NC}\n\v"

	if [ "$DE" == "GNOME" ]; then
		printf "${YC} WARNING: There is currently no easy way to disable composition or the gnome window manager. To be able to use TacO on GNOME, before each run, you have to logout of your current user session and then login again, choosing ${CC}openbox${YC} as the display manager. Then start GW2/TacO as you normally would.${NC}\n\v"
	fi
	if [ "$DE" == "i3wm" ] && [ "${s3}" -eq 1 ]; then
		printf "${YC} NOTE: i3wm does not have a compositor by default. Assuming picom.${NC}\n\v"
	fi
	if [ "$COMP" == "picom" ]; then
		printf "${YC} WARNING: You are using picom as a compositor. ${BC}run_taco${YC} will override any custom/default picom configurations. If you would like to use your own picom configuration instead of the provided ones, adjust the path for picom in ${BC}config/settings.conf${YC} or edit ${BC}config/picom.conf${YC}. When using ${BC}transparency${YC} mode some parameters will be forced regardless of configurations used.${NC}\n\v"
	fi
	if [ "$COMP" == "other" ]; then
		printf "${RC} ERROR: Only picom & native compositors are supported.\n\v  Exiting...${NC}\n\v"
		exit 2
	fi

	read -rep "Start with this info? (y/n): " -n 1 YESNO
	check_error "confirm" "$YESNO"

	taco_man
}
#------------------------------------------------------------------------#
#-------------------- #START INSTALLING DE TWEAKS-----------------------#
install_tweaks()
{
	if [ "${INSTALLED}" == "Installed" ]; then
		printf "${GC}** Skipping DE Tweaks because they are already installed...${NC}\n"
		sleep 2
	else
		printf "${GC}** Starting install of DE Tweaks...${NC}"
		
		case $DE in

			"XFCE") #---------------XFCE-----------------------
				printf "\nWe will now install & configure tweaks for the${GC} XFCE${NC} environment.\n\v"
				read -n 1 -s -r -p "Press any key to continue"

				if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
					printf "${GC}\n\v** Installing the ${CC}picom${GC} compositor ${NC}\n"
					sudo apt install picom xdotool
					check_error "apt" "$?"

				else # [ "$DIST" == "Arch/Manjaro" ]
					printf "${GC}\n\v** Installing the ${CC}Picom${GC} compositor ${NC}\n"
					sudo pacman -S --needed picom
					check_error "pacman" "$?"
				fi

				# Export Configs
				export_defaults
				check_error "tweaks" "$?"
			;;
			
			"KDE")  #---------------KDE-----------------------
				printf "\nWe will now install & configure tweaks for the ${GC}KDE${NC} environment.\n\v"
				read -n 1 -s -r -p "Press any key to continue"

				if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
					printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor, ${CC}xdotool${GC}...${NC}\n"
					sudo apt install picom xdotool
					check_error "apt" "$?"

				else # [ "$DIST" == "Arch/Manjaro" ]
					printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}xdotool${GC}... ${NC}\n"
					sudo pacman -S --needed picom xdotool
					check_error "pacman" "$?"
				fi

				# Check for previous kwinrules & set correct group
				printf "${GC}** Checking for any previous kwinrules... ${NC}\n"
				COUNT=$((`kreadconfig5 --file kwinrulesrc --group "General" --key count` + 1))
				check_error "tweaks" "$?"

				# ADD GW2 kwinrule keys
				printf "${GC}** Adding GW2 kwinrules ${NC}\n"
				kwriteconfig5 --file kwinrulesrc --group "General" --key count "$COUNT" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key Description "GW2 Opacity" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive "100" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactiverule "2" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key title "Guild Wars 2" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key titlematch "0" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclass "^(?!.*taco.*).*(?:gw2|wine).*" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclasscomplete "true" &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclassmatch "3"
				check_error "kwinrules" "$?"
				
				# Reset kwin
				printf "${GC}** Refreshing KWin Window Manager ${NC}\n"
				qdbus org.kde.KWin /KWin reconfigure
				check_error "tweaks" "$?"

				# Export configs
				export_defaults
				printf "${GC}** Exporting kwinrule count ${NC}\n"
				printf "\nkwinrule=$COUNT" >> "${DIR}/${SETTINGS}"
				check_error "tweaks" "$?"
			;;

			"Cinnamon") #---------------CINNAMON-----------------------
				printf "\nWe will now install & configure tweaks for the${GC} Cinnamon${NC} environment.\n\v"
				read -n 1 -s -r -p "Press any key to continue"

				if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
					printf "${GC}\n\v** Installing ${CC}picom${GC} compositor, ${CC}Openbox${GC}...${NC}\n"
					sudo apt install picom openbox xdotool
					check_error "apt" "$?"

				else # [ "$DIST" == "Arch/Manjaro" ]
					printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}Openbox${GC} window manager... ${NC}\n"
					sudo pacman -S picom openbox xdotool
					check_error "pacman" "$?"
				fi

				# Export configs
				export_defaults
			;;

			"GNOME") #--------------- GNOME -----------------------
				printf "\nWe will now install & configure tweaks for the${GC} GNOME${NC} environment.\n\v"
				read -n 1 -s -r -p "Press any key to continue"

				if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
					printf "${GC}\n\v** Installing ${CC}picom${GC} compositor, ${CC}Openbox${GC}...${NC}\n"
					sudo apt install picom openbox xdotool
					check_error "apt" "$?"

				else # [ "$DIST" == "Arch/Manjaro" ]
					printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}Openbox${GC} window manager... ${NC}\n"
					sudo pacman -S --needed picom openbox xdotool
					check_error "pacman" "$?"
				fi

				# Export configs
				export_defaults
			;;

			*)
				printf "\nYour Desktop Environment is currently not supported.\nTry installing manually or waiting for an update...\n\v"
				#rm ${SETTINGS}
				exit 2
			;;

		esac

		# Link run_taco.sh script to portable package
		if [ "$WINE" == "portable" ]; then
			printf "${GC}** Linking into ../bin/user_run... ${NC}\n"
			sed -i '$ s/$/\ \&\ \.\.\/\.\.\/\.\.\/gw2taco\/run_taco.sh/' ../bin/user_run
			check_error "tweaks" "$?"
		fi

		read -rep "Would you like TacO to start automatically with GW2? (y/n): " -n 1 AUTO
		check_error "autostart" "${AUTO}"

		sleep 2 #safety delay
		finish_install

	fi

	# Apply patches
	# fix_taco_trails()
	fix_transparent_css "app/${folder}"

	# Mark install
	printf "\n${GC}** Marking install...\n"
	sed -i "s/installed=.*/&${folder},/" ${SETTINGS}
	check_error "tweaks" "$?"

}
#------------------------------------------------------------------------#
#--------------------------- INSTALL PATCHES ---------------------------#

#--------------------------------------------------------------------------#
#-------------------- FINISH INSTALL & SET AUTOSTART ---------------------#
finish_install()
{
	show_header "tacoman" "complete"

	#PORTABLE
	if [ "$WINE" == "portable" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "\nTACO is set to autostart\n"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "\nTACO is set to NOT autostart.\nYou will first have to start GW2 and then use${YC} run_taco.sh${NC}"
		fi

		printf "\nTo change configurations, use option ${YC}[3]${NC} in TacoMan or edit${CC} ${SETTINGS}${NC} manually"
	
	fi

	#NATIVE
	if [ "$WINE" == "native" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "${RC}\nNOTE: Autostart for native WINE is experimental.${NC}\nYou will have to use run_taco.sh instead of starting GW2.exe, it will start it for you.\n If issues arise then set autostart=false and use${YC} run_taco.sh${NC} to activate taco AFTER having started GW2 and loaded a map"
			printf "\nTo change startup time & opacity, use option ${YC}[3]${NC} in TacoMan or edit${BC} ${SETTINGS}${NC} manually"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "${RC}\nNOTE: Native WINE support is currently experimental.${NC}\nIf issues arise please submit it on GitLab.\nTo activate taco, first start GW2, load a map and then use${YC} run_taco.sh${NC}"
			printf "\nTo change startup time & opacity, use option ${YC}[3]${NC} in TacoMan or edit${BC} ${SETTINGS}${NC} manually"
		fi

	fi

	#LUTRIS
	if [ "$WINE" == "lutris" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "\ngameid=0" >> ${SETTINGS}
			printf "${YC}\nNOTE: Lutris support is currently experimental. You must change gameid=0 to the gameid of guildwars2 in lutris.${NC}\nIf issues arise please submit it on GitLab.\nYou will have to use run_taco.sh, it will start GW2 for you via lutris it for you.\n If issues arise then set autostart=false and use${YC} run_taco.sh${NC} to activate taco AFTER having started GW2 and loaded a map"
			printf "\nTo change startup time & opacity, use option ${YC}[3]${NC} in TacoMan or edit${BC} ${SETTINGS}${NC} manually"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "\ngameid=0" >> ${SETTINGS}
			printf "${YC}\nNOTE: Lutris support is currently experimental. You must change gameid=0 to the gameid of guildwars2 in lutris if you want to use autostart.${NC}\nIf issues arise please submit it on GitLab.\nTo activate taco, first start GW2, load a map and then use${YC} run_taco.sh${NC}"
			printf "\nTo change startup time & opacity, use option ${YC}[3]${NC} in TacoMan or edit${BC} ${SETTINGS}${NC} manually"
		fi

	fi

	printf "${GC}\n-------------------------------------------------${NC}\n\v"

	read -n 1 -s -r -p "Press any key to continue"
	return
}
#-------------------------#

run_script()
{
	# check_error			# Error checker
	# export_defaults		# Exports user info & default configs
	# fix_transparent_css	# Fixes transparent taco menu
	# fix_taco_trails		# Installs d3dcompiler_47 to fix taco trails
	# install_tacopack		# Downloads, Installs & Activates tacopacks
	# show_header			# Shows the header
	# show_footer			# Shows the footer
	  check_install			# Checks if already installed and offers uninstall
	# collect_info			# Asks for environment info if not installed
	# taco_man				# Taco Manager
	# install_tweaks		# Starts installing tweaks based on environment
	# finish_install		# Finish install by exporting last configs & setting autostart=true/false
}
run_script # Initialize